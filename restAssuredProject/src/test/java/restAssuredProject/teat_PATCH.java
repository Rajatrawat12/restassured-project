package restAssuredProject;


	import java.util.HashMap;
	import java.util.Map;
	import static io.restassured.RestAssured.*;
	import org.json.simple.JSONObject;
	import org.testng.annotations.Test;

	import io.restassured.http.ContentType;

	public class teat_PATCH {
		@Test
		void test_01() {
//			Map <String, Object > map =  new HashMap <String,Object >();
//			map.put("name","Rajat");
//			map.put("job","Devloper");
			//System.out.println(map);
			JSONObject request = new JSONObject();
			request.put("name","Rajat");
			request.put("job","Devloper");
			System.out.println(request);
			System.out.println(request.toJSONString());
			
		
			given().
			header("content-type","application/json").
			contentType(ContentType.JSON).accept(ContentType.JSON).
			body(request.toJSONString()).
		
			when(). 
			patch("https://reqres.in/api/users/2").
		
			then().
			statusCode(200).
			log().all();
		}

}
